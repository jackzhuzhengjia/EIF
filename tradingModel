clear;clc;

%一、设置参数
group=10;%因子打分分组数，一般为5、10、20和50
date=84+14;%每月打分日，可取1-31日中任何一天,调仓日在打分日后一交易日
trading_cost=0.003;%交易成本(双向)，包括冲击成本
num=1;%在打分日求因子过去多少个交易日的平均值，一般为1日、5日和20日
mode1=2;%mode1=1包括上市未满1年的次新股，mode1=2去除上市未满1年的次新股
mode2=2;%mode2=1包括ST股票，mode2=2为去除ST股票
e=22+9;%动量反转效应形成期

%二、数据准备
load('hs300_2005_2019');%导入沪深300每日成分股数据，变量名为hs300
[closing_price_original,security_id]=xlsread('沪深300所有成分股股价汇总_因子测试',1);
opening_price=xlsread('沪深300所有成分股股价汇总_因子测试',2);
average_price=xlsread('沪深300所有成分股股价汇总_因子测试',3);
[~,ipo_date]=xlsread('沪深300上市时间',1);
hs300_index=xlsread('沪深300历史回报',1); %第一列日期，第二列收盘价
[~,~,hs300_delisting]=xlsread('沪深300退市股信息',1);
hs300_delisting(:,end)=[];%去掉最后一列NA列

closing_price=closing_price_original(length(closing_price_original(:,1))-length(opening_price(:,1))+1:end,:);
factor=NaN(size(closing_price,1),size(closing_price,2)+1);%为沪深300数据处理模块
factor(:,1)=m2xdate(datenum([hs300{:,1}])); %截取时间，转换成excel时间,第一列为日期
ST=factor;%所有数据维度应该和沪深300成分股数据closing_price一样,只保留factor和ST的日期列
hs300_index(:,1)=[];%去掉第一列(日期), 第二列为收盘价
security_id=security_id(2,2:end);%提取沪深300历史成分股代码，共789只
ipo_date=transpose(m2xdate(datenum(ipo_date(3,2:end))));%转换成excel serial date number

for i=1:size(factor,1)
    Location=find(ismember(security_id,hs300{i,2}(:,2)));
    for k=1:length(Location)
        factor(i,Location(k)+1)=closing_price(i,Location(k))/closing_price_original(size(closing_price_original,1)-size(closing_price,1)+i-e+1,Location(k))-1;
        if isequal(char(hs300{i,2}(k,5)),'否')
            ST(i,Location(k)+1)=0;
        else
            ST(i,Location(k)+1)=1;
        end
    end
end

trading_status=cell(size(factor,1),size(factor,2)-1); %注意在trading_status中，在第i天的股票不管是否为沪深300成分股，我们都需要记录
for i=1:size(average_price,1) %先记录成分股交易情况，我们首先知道停牌情况，从average_price中获取
    for j=1:size(average_price,2)
        if isnan(average_price(i,j))
            trading_status(i,j)={'停牌'};%只要有均价记录就是正常交易，否则就是停牌
        else
            trading_status(i,j)={'正常交易'};
        end
    end
end

for i=1:length(hs300_delisting(:,1))%如果是现金收购，处理收盘价，开盘价，均价，交易状态
    if strcmp(hs300_delisting(i,11),'现金')
        delisting_date=m2xdate(datenum(datetime(hs300_delisting(i,4))));%退市时间
        last_trading_date=m2xdate(datenum(datetime(hs300_delisting(i,6))));%最后交易日
        nonadjust_closing_price=cell2mat(hs300_delisting(i,7));%最后交易日未复权收盘价
        position=find(ismember(security_id,hs300_delisting(i,1)));%closing price中的股票位置
        adjust_closing_price=closing_price(factor(:,1)==last_trading_date,position);%最后交易日复权收盘价
        cash_price=cell2mat(hs300_delisting(i,10));%现金收购价格
        closing_price_adjusted=cash_price*(adjust_closing_price/nonadjust_closing_price);%退市日复权后收盘价=现金收购价格*(最后交易日复权后收盘价/最后交易日未复权收盘价)
        closing_price(find(factor(:,1)==delisting_date):end,position)=closing_price_adjusted;%在closing_price，opening，average中调整原股票复权后收盘价
        opening_price(find(factor(:,1)==delisting_date):end,position)=closing_price_adjusted;
        average_price(find(factor(:,1)==delisting_date):end,position)=closing_price_adjusted;
        trading_status(find(factor(:,1)==delisting_date):end,position)={'退市现金'};%在trading status中把退市之后的交易状态全部改成退市现金
    elseif strcmp(hs300_delisting(i,11),'转股')
        position1=find(ismember(security_id,hs300_delisting(i,1)));%原股票在closing price中的位置
        position2=find(ismember(security_id,hs300_delisting(i,9)));%新股票在closing price中的位置
        first_trading_date=m2xdate(datenum(datetime(hs300_delisting(i,12))));%新股票第一个交易日
        last_trading_date=m2xdate(datenum(datetime(hs300_delisting(i,6))));%最后交易日
        nonadjust_closing_price1=cell2mat(hs300_delisting(i,7));%原股票最后交易日未复权收盘价
        nonadjust_closing_price2=cell2mat(hs300_delisting(i,13));%新股票第一个交易日未复权收盘价
        nonadjust_opening_price=cell2mat(hs300_delisting(i,14));%新股票第一个交易日未复权开盘价
        nonadjust_average_price=cell2mat(hs300_delisting(i,15));%新股票第一个交易日未复权均价
        adjust_closing_price=closing_price(factor(:,1)==last_trading_date,position1);%原证券最后交易日复权收盘价
        transfer_rate=cell2mat(hs300_delisting(i,10));%转换比例
        %原股票第一个交易日复权价格=原股票最后交易日复权收盘价*(新股票第一个交易日未复权价格/原股票最后交易日未复权收盘价)*转换比例
        closing_price(factor(:,1)==first_trading_date,position1)=adjust_closing_price*(nonadjust_closing_price2/nonadjust_closing_price1)*transfer_rate;%原股票第一个交易日复权收盘价
        opening_price(factor(:,1)==first_trading_date,position1)=adjust_closing_price*(nonadjust_opening_price/nonadjust_closing_price1)*transfer_rate;%原股票第一个交易日复权开盘价
        average_price(factor(:,1)==first_trading_date,position1)=adjust_closing_price*(nonadjust_average_price/nonadjust_closing_price1)*transfer_rate;%原股票第一个交易日复权均价
        %新股票在第一个交易日之后的价格=原股票第一个交易日复权价格*新股票第一个交易日之后的价格/新股票第一个交易日的价格
        closing_price(find(factor(:,1)==first_trading_date)+1:end,position1)=closing_price(factor(:,1)==first_trading_date,position1)*closing_price(find(factor(:,1)==first_trading_date)+1:end,position2)/closing_price(factor(:,1)==first_trading_date,position2);%在closing_price，opening，average中调整原股票复权后收盘价
        opening_price(find(factor(:,1)==first_trading_date)+1:end,position1)=opening_price(factor(:,1)==first_trading_date,position1)*opening_price(find(factor(:,1)==first_trading_date)+1:end,position2)/opening_price(factor(:,1)==first_trading_date,position2);
        average_price(find(factor(:,1)==first_trading_date)+1:end,position1)=average_price(factor(:,1)==first_trading_date,position1)*average_price(find(factor(:,1)==first_trading_date)+1:end,position2)/average_price(factor(:,1)==first_trading_date,position2);
        trading_status(find(factor(:,1)==first_trading_date):end,position1)={'退市换股'};%在trading status中把退市之后(新股票第一个交易日)的交易状态全部改成退市换股
    end
end

score_date=NaN(length(factor(:,1)),2);%第一列为调仓日的日期，第二列为调仓日所在位置
score_date(1,1)=factor(1,1);
score_date(1,2)=1;
j=2;
for i=2:length(factor(:,1))-1
    if rem(i-1,date)==0%如果第i个交易日是调仓日的倍数的话
        score_date(j,1)=factor(i,1);%第一列储存日期
        score_date(j,2)=i;%第二列储存调仓日所在行数
        j=j+1;
    end
end
score_date(j,1)=factor(end,1);%将有因子值数据的最后一个交易日，计为最后一个打分日
score_date(j,2)=length(factor(:,1));
score_date(isnan(score_date(:,1)),:)=[];%只留下打分日的数据

year_end(:,1)=unique(year(x2mdate(score_date(:,1))));%记录每年的年末日
year_end(:,2)=12;
year_end(:,3)=31;
year_end=m2xdate(datenum(year_end));

year_end_date=NaN(length(year_end),1);%找出每年最后一个交易日
for i=1:length(year_end)
    for j=1:length(factor(:,1))-1
        if year_end(i,1)>=factor(j,1) && year_end(i,1)<factor(j+1,1)
            year_end_date(i,1)=factor(j,1);
        end
    end
end
year_end_date(end,1)=factor(end,1);%最后一个交易日作为最后一年的年末

a=size(factor);%a(1)行数，a(2)列数
if mode1==2
    for i=1:a(1) %剔除上市一年内的股票数据
        for j=2:a(2)
            if ipo_date(1,j-1)==factor(i,1) %IPO时间和因子时间相同时
                if i+243<=a(1) %判断上市以来交易日是否满一年
                    factor(1:i+243,j)=NaN;%满一年，将上市一年内的数据剔除（新上市波动较大）
                else
                    factor(1:a(1),j)=NaN;%i+243>行数，就是上市不满一年，将其数据全部变为NaN
                end
            end
        end
    end
end

if mode2==2
    factor(ST==1)=NaN; %去除ST股票的因子值
end

factor_adjust=NaN(a(1),a(2));%对factor进行调整，对其进行num个交易日平均
factor_adjust(:,1)=factor(:,1);%第一列为日期
for i=1:a(1) %计算平均值是否需要将NaN变为0，NaN与其它数值相加减最终结果仍为NaN，factor(isnan(factor))=0
    for j=2:a(2)
        if i-num+1>0
            factor_adjust(i,j)=mean(factor(i-num+1:i,j));%计算因子过去num个交易日的平均值
        end
    end
end

%三、对因子打分分组
b=length(score_date); %回测中的调仓次数
factor_in_score_date=factor_adjust(ismember(factor_adjust(:,1),score_date(:,1)),2:end);%取每月date日的数据作为该因子这一期的值，不包括日期列

%对因子按行（同一时期）进行升序排列（NaN的数据在最右边），同时获得同一行里面的数据在升序排序前的位置
[ascending_factor,factor_position]=sort(factor_in_score_date,2);%默认为升序
stock_amount=zeros(b,1);%每一期去掉NaN的数据后的股票个数（结果为一列），
for i=1:b
    stock_amount(i,1)=numel(ascending_factor(i,:))-numel(find(isnan(ascending_factor(i,:)))); %所有股票数-NaN股票数=有数据的股票数
end

i=1;%寻找第一期的股票数量超过group
j=0;
while i<=b && stock_amount(i,1)<group %对number（当期具有有效数据的股票数目）值前几期小于group的数据计数
    j=j+1;
    i=i+1;
end
adjust=j;%adjust为number中从第一行起连续小于group的行数

%由于factor以过去一年数据计算结果作为今天的结果,所以不考虑前（adjust-1）行为0的数据(不同指标由于计算方法不同，删除的行数也不同）
%对该因子进行打分，然后从低到高分成group组，记录每只股票排序前的位置和该股票该因子得分排在哪组
factor_score=NaN(b-adjust,a(2)-1);%b为score_date行数，a(2)所有股票因子数
for i=adjust+1:b
    for k=1:a(2)-1
        for j=1:group
            if stock_amount(i,1)>=group && k>ceil(stock_amount(i,1)/group*(j-1)) && k<=ceil(stock_amount(i,1)/group*j)
                %去除当期有因子值的股票数小于group的情况，将此行有数据的均分成group组
                factor_score(i-adjust,factor_position(i,k))=j;%按照初始位置，第i期第k只股票分在第几组
            end
        end
    end
end

if score_date(end,2)-score_date(end-1,2)<20 %如果最后一期的交易日数小于20，不予考虑
    factor_score_output=NaN(b-adjust-1,a(2));%用于因子间相关性测试
    factor_score_output(1:end,2:end)=factor_score(1:end-1,:);
    factor_score_output(:,1)=score_date(adjust+1:end-1,1);%最后一期交易不要
else
    factor_score_output=NaN(b-adjust,a(2));
    factor_score_output(1:end,2:end)=factor_score;
    factor_score_output(:,1)=score_date(adjust+1:end,1);%第一列为打分日日期
end

%计算每期每个股票的收益率（上月16日开盘价，本月15日收盘价之间的收益率）
closing_price_isd=NaN(b-adjust,a(2)-1);%opening_price_in_score_date
opening_price_asd=NaN(b-adjust-1,a(2)-1);%opening_price_after_score_date
closing_price_asd=NaN(b-adjust-1,a(2)-1);%closing_price_after_score_date
average_price_asd=NaN(b-adjust-1,a(2)-1);%average_price_after_score_date
for k=adjust+1:b
    for i=1:a(1)
        if score_date(k,1)==factor_adjust(i,1) %score_date第二列记录打分日在factor中的行数
            for j=1:a(2)-1
                if i<a(1)
                    closing_price_isd(k-adjust,j)=closing_price(i,j);%记录打分日的收盘价
                    opening_price_asd(k-adjust,j)=opening_price(i+1,j);%开盘价为打分日第二日的开盘价
                    closing_price_asd(k-adjust,j)=closing_price(i+1,j);%收盘价为打分日第二日的收盘价
                    average_price_asd(k-adjust,j)=average_price(i+1,j);%均价为打分日第二日的均价
                else
                    closing_price_isd(k-adjust,j)=closing_price(i,j);%避免最后一期opening_price(i+1,j)出现溢出
                end
            end
        end
    end
end

stock_return_day_1=opening_price_asd(1:b-adjust-1,:)./closing_price_isd(1:b-adjust-1,:);%计算打分日收盘价到第二日开盘价间的收益率
stock_return_day_2=closing_price_asd(1:b-adjust-1,:)./closing_price_isd(1:b-adjust-1,:);%计算打分日收盘价到第二日收盘价之间的收益率
stock_return_day_3=average_price_asd(1:b-adjust-1,:)./closing_price_isd(1:b-adjust-1,:);%计算打分日收盘价到第二日均价间的收益率
stock_return_day_4=closing_price_asd(1:b-adjust-1,:)./average_price_asd(1:b-adjust-1,:);%计算打分日第二日均价到第二日收盘价间的收益率
stock_return_month=closing_price_isd(2:b-adjust,:)./closing_price_asd(1:b-adjust-1,:);%计算上期打分日第二日收盘价到本期打分日收盘价之间的收益率

%四、计算每组累计收益率分布
s1.stock_id={};
s1.stock_position=[];
s1.stock_value=[];%记录每只股票的持有市值
s1.stock_status={};%判断股票状态
pisd=repmat(s1,b-adjust,group);%pisd=portfolio_in_score_date
s2.stock_id={};
s2.stock_position=[];
s2.stock_value=[];
s2.proportion=[];%记录需加仓的股票间应买入的比例
pasd=repmat(s2,b-adjust-1,group);%pasd=portfolio_after_score_date

%停牌指停牌一天、连续停牌；未停牌指停牌一个小时、停牌半天、停牌半小时、复牌、正常交易、盘中停牌；退市指暂停上市、未上市
for i=1:group %pisd第一行赋值
    pisd(1,i).stock_id={'Cash'};%设置初始组合净值为1
    pisd(1,i).stock_position=-1;
    pisd(1,i).stock_value=1;
    pisd(1,i).stock_status={'正常交易'};
end

%在打分日收盘后进行因子打分
for i=1:b-adjust-1
    for j=1:group %先对pasd进行赋值
        if stock_amount(i+adjust,1)<group %当有因子值的股票数小于group时不调仓
            pasd(i,j).stock_id=pisd(i,j).stock_id;
            pasd(i,j).stock_position=pisd(i,j).stock_position;
            pasd(i,j).stock_value=pisd(i,j).stock_value.*stock_return_day_2(i,pisd(i,j).stock_position);%打分日后一天的股票市值=打分日当天持有的股票净值*打分日后一天的股票收益率
        else
            pasd(i,j).stock_id=security_id(factor_score(i,:)==j);%对pasd的stock_id、position进行赋值
            pasd(i,j).stock_position=find(factor_score(i,:)==j);
            length_pasd=length(pasd(i,j).stock_position);
            average_pisd_value=sum(pisd(i,j).stock_value)/length_pasd;
            pasd(i,j).stock_value=zeros(1,length_pasd);%对pasd的stock_value进行赋值
            pasd(i,j).proportion=zeros(1,length_pasd);
            sum_sell=0;%卖出之后持有的现金数量, 用来买入
            for m=1:length_pasd %避免打分日是沪深300成分股，而第二日是此股退市日的情况
                if strcmp(trading_status(score_date(i+adjust,2)+1,pasd(i,j).stock_position(m)),'退市换股')
                    pasd(i,j).stock_id(m)=hs300_delisting(ismember(hs300_delisting(:,1),pasd(i,j).stock_id(m)),9);%修改股票代码
                    pasd(i,j).stock_position(m)=find(strcmp(security_id,pasd(i,j).stock_id(m)));%修改新股票在closing_price中位置
                end
            end
            
            for k=1:length(pisd(i,j).stock_position) %先考虑卖出
                if isempty(pasd(i,j).stock_position(pasd(i,j).stock_position==pisd(i,j).stock_position(k)))%上次持有，这次不持有，需卖出
                    if i==1
                        sum_sell=sum_sell+pisd(i,j).stock_value(k);
                    elseif strcmp(pisd(i,j).stock_status(k),'退市现金')%如果为现金收购的话
                        sum_sell=sum_sell+pisd(i,j).stock_value(k);%卖出所得为之前在closing_price中计算完之后的现金价格
                    elseif strcmp(pisd(i,j).stock_status(k),'停牌')%记录对应位置因为停牌无法卖出，所以仍需持有。
                        pasd(i,j).stock_id(length(pasd(i,j).proportion)+1)=pisd(i,j).stock_id(k);
                        pasd(i,j).stock_position(length(pasd(i,j).proportion)+1)=pisd(i,j).stock_position(k);%在对应pasd.stock_id对应(i,j)位置的末尾加上这支停牌股在pisd(i,j)中的对应位置
                        pasd(i,j).stock_value(length(pasd(i,j).proportion)+1)=pisd(i,j).stock_value(k);%停牌无法卖出仍需持有
                        pasd(i,j).proportion(length(pasd(i,j).proportion)+1)=0;
                    elseif stock_return_day_1(i,pisd(i,j).stock_position(k))<=0.91 %记录对应位置因为跌停无法卖出所以仍需持有，定义第二日开盘价跌9%为跌停
                        pasd(i,j).stock_id(length(pasd(i,j).proportion)+1)=pisd(i,j).stock_id(k);
                        pasd(i,j).stock_position(length(pasd(i,j).proportion)+1)=pisd(i,j).stock_position(k);%在对应pasd.stock_id对应(i,j)位置的末尾加上这支跌停股在pisd(i,j)中的对应位置
                        pasd(i,j).stock_value(length(pasd(i,j).proportion)+1)=pisd(i,j).stock_value(k)*stock_return_day_2(i,pisd(i,j).stock_position(k));%停牌无法卖出仍需持有
                        pasd(i,j).proportion(length(pasd(i,j).proportion)+1)=0;
                    else
                        sum_sell=sum_sell+pisd(i,j).stock_value(k)*stock_return_day_3(i,pisd(i,j).stock_position(k))*(1-trading_cost);%计算卖掉的市值，全卖掉
                    end
                else %上次持有，这次也持有
                    if strcmp(pisd(i,j).stock_status(k),'退市现金')%如果为现金收购的话
                        sum_sell=sum_sell+pisd(i,j).stock_value(k);%卖出所得为之前在closing_price中计算完之后的现金价格
                    elseif strcmp(pisd(i,j).stock_status(k),'停牌')
                        pasd(i,j).stock_value(pasd(i,j).stock_position==pisd(i,j).stock_position(k))=pisd(i,j).stock_value(k);
                    elseif pisd(i,j).stock_value(k)>=average_pisd_value %需要卖出部分持仓
                        if stock_return_day_1(i,pisd(i,j).stock_position(k))<=0.91 %遇到跌停无法卖出
                            pasd(i,j).stock_value(pasd(i,j).stock_position==pisd(i,j).stock_position(k))=pisd(i,j).stock_value(k)*stock_return_day_2(i,pisd(i,j).stock_position(k));
                        else
                            sum_sell=sum_sell+(pisd(i,j).stock_value(k)-average_pisd_value)*stock_return_day_3(i,pisd(i,j).stock_position(k))*(1-trading_cost);
                            pasd(i,j).stock_value(pasd(i,j).stock_position==pisd(i,j).stock_position(k))=average_pisd_value*stock_return_day_2(i,pisd(i,j).stock_position(k));
                        end
                    elseif pisd(i,j).stock_value(k)<average_pisd_value && stock_return_day_1(i,pisd(i,j).stock_position(k))>=1.09 %需要增加持仓，但遇到涨停，定义第二日开盘价涨9%为涨停
                        pasd(i,j).stock_value(pasd(i,j).stock_position==pisd(i,j).stock_position(k))=pisd(i,j).stock_value(k)*stock_return_day_2(i,pisd(i,j).stock_position(k));
                    else
                        pasd(i,j).stock_value(pasd(i,j).stock_position==pisd(i,j).stock_position(k))=pisd(i,j).stock_value(k)*stock_return_day_2(i,pisd(i,j).stock_position(k));
                        pasd(i,j).proportion(pasd(i,j).stock_position==pisd(i,j).stock_position(k))=(average_pisd_value-pisd(i,j).stock_value(k))/average_pisd_value;
                    end
                end
            end
            
            for k=1:length_pasd %再考虑买入
                if isempty(pisd(i,j).stock_position(pisd(i,j).stock_position==pasd(i,j).stock_position(k)))%这次持有，上次不持有，需买入
                    if ~strcmp(trading_status(score_date(i+adjust,2)+1,pasd(i,j).stock_position(k)),'停牌') && stock_return_day_1(i,pasd(i,j).stock_position(k))<1.09 %判断的是打分日第二日的交易状态
                        pasd(i,j).proportion(k)=1/length_pasd;
                    end
                end
            end
            
            c=sum(pasd(i,j).proportion);%对买入进行赋值
            for k=1:length_pasd
                if pasd(i,j).proportion(k)>0
                    pasd(i,j).stock_value(k)=pasd(i,j).stock_value(k)+pasd(i,j).proportion(k)/c*sum_sell*stock_return_day_4(i,pasd(i,j).stock_position(k))*(1-trading_cost);
                end
            end
            
            h=zeros(1,length(pasd(i,j).stock_value));
            for k=1:length(pasd(i,j).stock_value)
                if pasd(i,j).stock_value(k)==0 %记录上次不持有、这次持有但无法买入的股票
                    h(1,k)=1;
                end
            end
            pasd(i,j).stock_id(logical(h))=[];%将上次不持有、这次持有但无法买入的股票去除
            pasd(i,j).stock_position(logical(h))=[];
            pasd(i,j).stock_value(logical(h))=[];
            pasd(i,j).proportion(logical(h))=[];
        end
        
        pisd(i+1,j).stock_id=pasd(i,j).stock_id;%再对pisd赋值
        pisd(i+1,j).stock_position=pasd(i,j).stock_position;
        pisd(i+1,j).stock_value=pasd(i,j).stock_value.*stock_return_month(i,pasd(i,j).stock_position);%根据打分日收盘价，持有某只股票的市值=这支股票上个月调仓完的净值*这只股票上个月的收益率
        if i<b-adjust-1 %下面判断pisd持有的股票第二日的交易状态，区分停牌，未停牌，退市现金，退市换股.
            for n=1:length(pisd(i+1,j).stock_position)
                if strcmp(trading_status(score_date(i+1+adjust,2)+1,pisd(i+1,j).stock_position(n)),'停牌') %判断的是打分日第二日的交易状态
                    pisd(i+1,j).stock_status(n)={'停牌'};
                elseif strcmp(trading_status(score_date(i+1+adjust,2)+1,pisd(i+1,j).stock_position(n)),'退市现金')
                    pisd(i+1,j).stock_status(n)={'退市现金'};%修改交易状态
                elseif strcmp(trading_status(score_date(i+1+adjust,2)+1,pisd(i+1,j).stock_position(n)),'退市换股')
                    pisd(i+1,j).stock_id(n)=hs300_delisting(ismember(hs300_delisting(:,1),pisd(i+1,j).stock_id(n)),9);%修改股票代码
                    pisd(i+1,j).stock_position(n)=find(strcmp(security_id,pisd(i+1,j).stock_id(n)));%修改新股票在closing_price中位置
                    pisd(i+1,j).stock_status(n)=trading_status(score_date(i+1+adjust,2)+1,pisd(i+1,j).stock_position(n));%修改交易状态
                else
                    pisd(i+1,j).stock_status(n)={'正常交易'};
                end
            end
        end
    end
end

%检查持仓情况
%1.检查pisd最后一期是否有未调整股票(check)
%3.检查trading_status中交易状态修改
%4.检查closing_price中价格修改是否正确
%5.检查opening_price中价格修改是否正确
%6.检查average_price中价格修改是否正确


figure(1);
group_total_return=NaN(1,group);%记录各组的累计总收益率
for i=1:group
    group_total_return(1,i)=sum(pisd(b-adjust,i).stock_value)-1;
end
bar(1:group,group_total_return);%画出各组累计总收益率的柱状图

%五、计算每一组（共group组）的每个交易日净值
daily_value=NaN(score_date(end,2)-score_date((adjust+1),2)+1,group);%每期每日净值，从第一个打分日算起
daily_value(1,:)=1;
for i=1:score_date(end,2)-score_date((adjust+1),2)
    for j=1:group
        for k=1:b-adjust-1
            if i+score_date(adjust+1,2)-1>=score_date(k+adjust,2) && i+score_date(adjust+1,2)-1<score_date(k+adjust+1,2)
                daily_value(i+1,j)=sum(pasd(k,j).stock_value.*closing_price(i+score_date(adjust+1,2),pasd(k,j).stock_position)./closing_price_asd(k,pasd(k,j).stock_position));
            end
        end
    end
end

figure(2);%画每组的收益率曲线
plot(factor(score_date(adjust+1,2):score_date(end,2),1),daily_value);%以时间为x轴，累计收益为y轴
xlabel('日期');
ylabel('每组净值');
l=1:group;
lable=cell(1,group);%建立各曲线标签
for i=1:group
    lable{1,i}=num2str(l(i));
end
legend(lable);%给各曲线加标签
title('净值曲线');
start_date=datestr(x2mdate(score_date(adjust+1,1),0));%求得第一次进行打分的日期，将Excel日期转为MATLAB日期
dateaxis('x', 2, start_date);%Convert serial-date axis labels to calendar-date axis labels

%六、收益排名稳定性
daily_return=daily_value(2:end,:)./daily_value(1:end-1,:)-1;%每组每日的收益率
[~,daily_return_position]=sort(daily_return,2);%对每日每组的收益率进行升序排名，并获得排序前的位置
daily_return_score=NaN(score_date(end,2)-score_date(adjust+1,2),group);
for i=1:score_date(end,2)-score_date(adjust+1,2)
    for k=1:group
        daily_return_score(i,daily_return_position(i,k))=k;%记录每组每日收益率的排名
    end
end
group_var=var(daily_return_score);%计算每组每日排名的方差
total_var=sum(group_var);%计算各组每日排名的总方差

hs300_daily_return=NaN(score_date(end,2)-score_date(adjust+1,2),1);%沪深300每日的收益率
for i=1:score_date(end,2)-score_date(adjust+1,2)
    %(后一天收盘价/前一天收盘价) - 1 = 当天收益
    hs300_daily_return(i,1)=hs300_index(score_date(adjust+1,2)+i)/hs300_index(score_date(adjust+1,2)+i-1)-1;
end
hs300_daily_value=NaN(score_date(end,2)-score_date(adjust+1,2)+1,1);
hs300_daily_value(1,1)=1;
for i=1:score_date(end,2)-score_date(adjust+1,2)
    hs300_daily_value(i+1,1)=hs300_daily_value(i,1)*(1+hs300_daily_return(i,1));%沪深300每日净值
end
win_ratio=sum(daily_return(:,(group_total_return==max(group_total_return)))>hs300_daily_return(:,1))...
    /(score_date(end,2)-score_date(adjust+1,2));%计算日胜率
hs300_total_teturn=hs300_daily_value(end)-1;%沪深300的累计收益率

figure(3);%画出收益最高的那一组与wind全A净值走势的比较图
compare_best_hs300=[hs300_daily_value,daily_value(:,(group_total_return==max(group_total_return)))];%将沪深300指数与表现最好的那组放在一起
plot(factor(score_date(adjust+1,2):score_date(end,2),1),compare_best_hs300);%以时间为x轴，累计收益为y轴
xlabel('Date');
ylabel('Net Value');
label=cell(1,2);%建立各曲线标签
label{1,1}='CSI300';
label{1,2}=num2str(find(group_total_return==max(group_total_return)));%返回最优组的组号
legend(label);%给各曲线加标签
title('Compare CSI 300 return with the best portfolio return, information_ratio:0.9089, annualized rate of return portfolio vs index:23.47% vs 11.51%, excess return:20.48%, maximum portfolio vs index drawdown: 67.10% vs 72.04%, tracking error:0.0085, conclusion: the currrent portfolio displays strong price momentum effect');
dateaxis('x', 2, start_date); %Convert serial-date axis labels to calendar-date axis labels

%七、统计每年各组表现的排名
d=sum(year_end_date<score_date(adjust+1,1));%计算小于第一个打分日间的年末数
f=sum(year_end_date>=score_date(adjust+1,1))-sum(year_end_date>score_date(end,1));
each_year_value=NaN(f+1,group);%计算第一个和最后一个打分日间的年末数
hs300_year_value=NaN(f+1,1);
each_year_value(1,:)=1;
hs300_year_value(1,1)=hs300_daily_value(1,1);
for i=1:f
    for j=1:group
        each_year_value(i+1,j)=daily_value(find(factor(:,1)==year_end_date(d+i,1))-score_date(adjust+1,2)+1,j);
        %记录每组每年末最后一个交易日的净值
    end
end
for i=1:f
    hs300_year_value(i+1,1)=hs300_daily_value(find(factor(:,1)==year_end_date(d+i,1))-score_date(adjust+1,2)+1,1);
    %记录指数每年末最后一个交易日的净值
end
each_year_return=each_year_value(2:end,:)./each_year_value(1:end-1,:)-1;
%每组每年的收益率，第一期是从第一个打分日到当年末的收益率，最后一期为最后一年年初到最后一个打分日的收益率
hs300_year_return=hs300_year_value(2:end,1)./hs300_year_value(1:end-1,1)-1;%指数每年收益率
[~,each_year_return_position]=sort(each_year_return,2);%对每组每年的收益率进行升序排名，并获得排序前的位置
each_year_return_score=NaN(d,group);
for i=1:f
    for k=1:group
        each_year_return_score(i,each_year_return_position(i,k))=k;%记录每组每年的收益率排名
    end
end

%八、IC测试
if score_date(end,2)-score_date(end-1,2)<20 %如果最后一期的交易日数小于20，不予考虑
    g=b-adjust-2;
else
    g=b-adjust-1;
end

group_return=NaN(g,group);%记录各组每期的收益率
for i=1:group
    for j=1:g
        group_return(j,i)=daily_value(score_date(adjust+1+j,2)-score_date(adjust+1,2)+1,i)/daily_value(score_date(adjust+j,2)-score_date(adjust+1,2)+1,i)-1;
    end
end
[~,group_return_position]=sort(group_return,2);%对每组每期收益率进行升序排名，并获得排序前的位置

y=repmat((1:group),g,1);%产生因子打分矩阵
regress_y=y(:);%收益率排名作为y，列排列
regress_x1=[ones(length(regress_y),1),group_return_position(:)];%因子打分排名作为x，列排列
[coef1,coef_interval1,resi1,resi_interval1,stats1] = regress(regress_y,regress_x1);%做线性回归
R_square1=stats1(1,1);
F_value1=stats1(1,2);
P_value1=stats1(1,3);
error_variance1=stats1(1,4);
figure(4);
plot((1:group)',coef1(2,1)*(1:group)'+coef1(1,1)*ones(group,1),'o',regress_x1(:,2),regress_y,'k+');

regress_x2=[ones(length(regress_y),1),group_return_position(:),group_return_position(:).^2];%因子打分排名作为x，列排列
[coef2,coef_interval2,resi2,resi_interval2,stats2] = regress(regress_y,regress_x2);%做二次回归
R_square2=stats2(1,1);
F_value2=stats2(1,2);
P_value2=stats2(1,3);
error_variance2=stats2(1,4);
figure(5);
plot((1:group)',coef2(3,1)*((1:group).^2)'+coef2(2,1)*(1:group)'+coef2(1,1)*ones(group,1),'o',regress_x2(:,2),regress_y,'k+');

%九、计算该指标最优组与万得全A的信息比率，以此作为计算每个因子的权重
group_yearly_return=(group_total_return(1,:)+1).^(365/(score_date(end,1)-score_date(adjust+1,1)))-1;%计算每组的年化收益率
hs300_yearly_return=(hs300_index(factor(:,1)==score_date(end,1),1)/hs300_index(factor(:,1)==score_date(adjust+1,1),1))^...
    (365/(score_date(end,1)-score_date(adjust+1,1)))-1;%计算沪深300的年化收益率
excess_return=max(group_yearly_return)-min(group_yearly_return); %计算该指标最优组与最差组的差值（超额收益）
[best_excess_hs300,best_return_position]=max(group_yearly_return-hs300_yearly_return);
tracking_error=std(daily_return-hs300_daily_return);%跟踪误差
information_ratio=best_excess_hs300./tracking_error(best_return_position)/(242^0.5);%最优组的信息比率


%计算最高收益率组最大回撤率以及指数最大回撤率
%最大回撤率= max(1 - 账户当日价值/当日之前账户最高价值)*100%
best_performing_group_daily_value = daily_value(:,best_return_position);
portfolio_drawdown=NaN(length(best_performing_group_daily_value)+1,1);
index_drawdown=NaN(length(hs300_daily_value)+1,1);
for i=1:length(best_performing_group_daily_value)
    portfolio_drawdown(i)=1-(best_performing_group_daily_value(i)/max(best_performing_group_daily_value(1:i)));
    index_drawdown(i)=1-(hs300_daily_value(i)/max(hs300_daily_value(1:i)));
end
max_portfolio_drawdown=max(portfolio_drawdown);
max_index_drawdown=max(index_drawdown);




